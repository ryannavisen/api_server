angular.module('app').controller('dashboard.controller', [
    '$scope', 'http.service', 'APIURL', 'toast.service', '$uibModal', '$rootScope',
    'baseUrl', '$cookies',
    function ($scope, httpService, APIURL, toastService, $uibModal, $rootScope, baseUrl,
        $cookies) {
        $scope.current_lang = $rootScope.lang.current;
        $scope.domains = [];
        $scope.url = baseUrl + APIURL.build + '/';
        $scope.accessToken = '?Authorization=' + $cookies.get('access_token') || '';
        $scope.noItems = false;
        $scope.allowAdd = false;

        $scope.getDomains = function () {
            httpService.doGraphQL(APIURL.query, 
            $scope.current_lang, 
            'ListDomains', {
            },[
                              
            ]).then(function (domains) {
                $scope.domains = domains;
                if (domains && domains.length < 1) {
                    $scope.noItems = true;
                }
                $scope.getAllowDomains();
            })
        };

        $scope.upgrade = function (id) {
            httpService.doGraphQL(APIURL.query, 
                $scope.current_lang, 'UpgradeDomain', {
                    id: id
                },[]).then(function (result) {
                    if (result.error) {
                        toastService.error(result.message.id[0]);  
                    } else {
                        toastService.success($rootScope.lang[$scope.current_lang]['Upgraded']);
                        $scope.getDomains();
                    }
            });
        };
        $scope.buildDomain = function () {};
        $scope.getAllowDomains = function () {
            httpService.doGraphQL(APIURL.query, 
                $scope.current_lang, 
                'AllowDomains', {},[]).then(function (allowDomain) {
                    if (allowDomain.max && ($scope.domains.length <= allowDomain.max)) {
                        $scope.allowAdd = true;
                    }
                });
        };

        $scope.getDomains();
    }]);