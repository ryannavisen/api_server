angular.module('app').controller('domain.add.controller', [
	'$scope', '$rootScope',  'http.service', 'APIURL', 'toast.service', '$state',
function ($scope, $rootScope, httpService, APIURL, toastService, $state) {
	$scope.current_lang = $rootScope.lang.current;
	$scope.form = {
        domain: ''
	};
    $scope.regex = "^(http[s]?:\\/\\/){0,1}(www\\.){0,1}[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,5}[\\.]{0,1}";    

	$scope.submitForm = function (isValid) {
		var parameters = $scope.form;
        httpService.doGraphQL(APIURL.query, 
        $scope.current_lang, 
        'AddDomain', {
			'domain': $scope.form.domain
        }, []).then(function (result) {
            if (result.error) {
                // console.log(result.message);
                toastService.error(result.message.domain[0]);    
            } else {
				toastService.success($rootScope.lang[$scope.current_lang]['Success']);
				$state.go('layout.dashboard');
            }
        })
	}
 }]);