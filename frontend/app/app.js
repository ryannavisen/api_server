angular.module('app', [
 'ui.router',
 'ui.bootstrap',
 'toastr',
 'ngCookies',
 'templates',
]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 
function($stateProvider, $urlRouterProvider, $httpProvider){
    $stateProvider.state('logout', {
        url : '/logout',
        templateUrl: 'login/logout.html',
        controller: 'logout.controller'
    });    
    $stateProvider.state('login', {
        url : '/login',
        templateUrl: 'login/login.html',
        controller: 'login.controller'
    });
    $stateProvider.state('layout', {
        templateUrl: 'layout.html'
    });
    $stateProvider.state('layout.dashboard', {
        url : '/dashboard',
        templateUrl: 'dashboard/dashboard.html',
        controller: 'dashboard.controller'
    });    
    $stateProvider.state('layout.domainAdd', {
        url : '/domain/add',
        templateUrl: 'dashboard/domainAdd.html',
        controller: 'domain.add.controller'
    });            
    $stateProvider.state('layout.profile', {
        url : '/profile',
        templateUrl: 'profile/profile.html',
        controller: 'profile.controller'
    });    
    $urlRouterProvider.otherwise('/login');

    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.interceptors.push(['$q', '$location', '$cookies', 'toast.service', 
    function ($q, $location, $cookies, toastService) {
    return {
        'request': function (config) {
            config.headers = config.headers || {};
            const token = $cookies.get('access_token') || '';
            if (token.length > 0) {
                config.headers.Authorization = token;
            }
            return config;
        },
        'responseError': function (response) {
            console.log('error response', response);
            if (response.status == 401 || response.status == -1) {
                $location.path('/login');
                toastService.error('You have been disconnected or you do not have access to this portal. Please log in again.');
            }
            return $q.reject(response);
        }
    };
    }]);    
}])
// .constant('baseUrl', 'http://localhost:8887/api')
.constant('baseUrl', 'http://api.navisen.com/api')
// .constant('env', 'dev')
.constant('env', 'prod')
.constant('APIURL', {
    login: '/login',
    query: '/query',
    build: '/build'
    
})
.run(['$rootScope', '$cookies', function ($rootScope, $cookies) {
    $rootScope.lang = {
        en: en,
        current: 'en'
    };

    $rootScope.toggle = false;

    if ($cookies.get('lang')) {
        $rootScope.lang.current = $cookies.get('lang');
    } else {
        $cookies.put('lang', $rootScope.lang.current);
    }
}]);