angular.module('app').controller('navigation.top.controller', [
    '$scope', '$cookies','$rootScope',  '$window',
function ($scope, $cookies, $rootScope, $window) {
    $scope.current_lang = 'en';//$cookies.get('lang');

    $scope.changeLanguage = function() {
        $rootScope.lang.current = $scope.current_lang;
        $cookies.put('lang', $rootScope.lang.current);
        $window.location.reload();
    };

    $scope.toggleSidebar = function () {
        $rootScope.toggle = !$rootScope.toggle;
    };
}]);
