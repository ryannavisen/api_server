angular.module('app').controller('login.controller', [
    '$scope', 'http.service', 'APIURL', 'toast.service', '$uibModal', '$state', '$cookies',
    '$rootScope', 
function ($scope, httpService, APIURL, toastService, $uibModal, $state, $cookies, $rootScope) {
    $scope.current_lang = $rootScope.lang.current;
    $scope.form = {
        email: '',
        password: ''
	};

	$scope.submitForm = function (isValid) {
		if (!$scope.loginForm.$invalid) {
			httpService.doPost(APIURL.login, $scope.form).then(function(result) {
                if (result.error) {
                    // console.log(result.message);
                    toastService.error('Invalid Credential');    
                } else {
                    toastService.success($rootScope.lang[$scope.current_lang]['Connected']);
                    $cookies.put('access_token', result.access_token);
                    $state.go('layout.dashboard');
                }
            });
		}
	}
}]);
