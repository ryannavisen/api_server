angular.module('app').controller('logout.controller', ['$scope', '$state', '$cookies',
function ($scope, $state, $cookies) {
    $cookies.remove('access_token');
    $state.go('login');
}]);
