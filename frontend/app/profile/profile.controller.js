angular.module('app').controller('profile.controller', [
	'$scope', '$rootScope',  'http.service', 'APIURL', 'toast.service',
function ($scope, $rootScope, httpService, APIURL, toastService) {
	$scope.current_lang = $rootScope.lang.current;
	$scope.profile = null;
	$scope.plan = null;
	$scope.form = {
		email: '',
		first_name: '',
		last_name: ''
    };
    
	$scope.submitForm = function (isValid) {
		var parameters = $scope.form;
        httpService.doGraphQL(APIURL.query, 
        $scope.current_lang, 
        'EditProfile', {
			'first_name': $scope.form.first_name,
			'last_name': $scope.form.last_name,
			'email': $scope.form.email,
			'password': $scope.form.password        
        }, []).then(function (result) {
            if (result.error) {
                var message = '';
                result.message.forEach(function(element) {
                    message = message + element.join(' ');
                }, this);
                toastService.error(message);    
            } else {
                toastService.success($rootScope.lang[$scope.current_lang]['Success']);
                $scope.getProfile();
            }
        })
	}

	$scope.getProfile = function (){
		httpService.doGraphQL(APIURL.query, 
			$scope.current_lang, 
			'GetProfile', {}, [
				'id',
				'first_name',
				'last_name',
				'email',
				'created_at'
			]).then(function (user) {
				$scope.profile = user;
				$scope.form.first_name = $scope.profile.first_name;
				$scope.form.last_name = $scope.profile.last_name;
				$scope.form.email = $scope.profile.email;
				$scope.form.created   = new Date($scope.profile.created).toISOString().slice(0, 10);
			})	
    };
    
    $scope.getPlan = function (){
		httpService.doGraphQL(APIURL.query, 
			$scope.current_lang, 
			'GetPlan', {}, []).then(function (plan) {
				$scope.plan = plan;
			})	
	};    

	$scope.getProfile();
	$scope.getPlan();
 }]);