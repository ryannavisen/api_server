angular.module('app').service('toast.service', ['toastr', function (toastr) {
    this._msg = function (type, msg) {
        if(!msg) { 
            msg = 'Salvado';
        }
        toastr[type](msg);
    };

    this.success = function (msg) {
        this._msg('success', msg);
    };
    
    this.error = function (msg) {
        this._msg('error', msg);
    };        
    return this;    
}]);