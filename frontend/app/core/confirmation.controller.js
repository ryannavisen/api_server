angular.module('app').controller('confirmation.controller', ['$uibModalInstance', '$scope', 
 function($uibModalInstance, $scope) {

	$scope.submit = function() {
		$uibModalInstance.close(true);
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
 }]);