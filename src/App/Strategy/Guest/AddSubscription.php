<?php namespace App\Strategy\Guest;

use App\Repository\Settings;

class AddSubscription {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $setting = new Settings();
        $result = $setting->getWhere ('name', 'maintenance');
        return [
            'operation' => ($result['value'] == '1') ? true : false
        ];
    }
}