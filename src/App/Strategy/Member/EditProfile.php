<?php namespace App\Strategy\Member;

use App\Repository\User;
use Symfony\Component\Validator\Constraints as Assert;

class EditProfile {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'email' => isset($parameter['email']) ? $parameter['email'] : '',
            'password' => isset($parameter['password']) ? $parameter['password'] : '',
            'first_name' => isset($parameter['first_name']) ? $parameter['first_name'] : '',
            'last_name' => isset($parameter['last_name']) ? $parameter['last_name'] : ''
        ];

        $constraint = new Assert\Collection(array(
            'email' => [
                new Assert\NotBlank(array('message' => 'email.short')),
                new Assert\Email(array(
                    'message' => 'email.invalid'
                ))
            ],
            'password' => [
                new Assert\Length(array('min' => 8, 'max' => 12, 'minMessage' => 'password.short', 'maxMessage' => 'password.long')),
                new Assert\Regex(array('pattern' => "/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/", 'message' => 'password.invalid'))
            ],            
            'first_name' => [
                new Assert\NotBlank(array('message' => 'first_name.short')),
                new Assert\Length(array('min' => 1, 'max' => 30, 'minMessage' => 'first_name.short', 'maxMessage' => 'first_name.long'))
            ],
            'last_name' => [
                new Assert\NotBlank(array('message' => 'last_name.short')),
                new Assert\Length(array('min' => 1, 'max' => 30, 'minMessage' => 'last_name.short', 'maxMessage' => 'last_name.long'))
            ]
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            if (strlen($data['password']) < 1) {
                unset($data['password']);
            } 
            $data['updated'] = date('Y-m-d h:m:s');
            $model = new User();
            $result = $model->customUpdate($userId, $data);
        }

        return $result;
    }
}