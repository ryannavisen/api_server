<?php namespace App\Strategy\Member;

use App\Repository\Domains;
use App\Repository\UserProfile;
use App\Repository\Plan;
use App\Repository\Codebase;
use Symfony\Component\Validator\Constraints as Assert;

class AllowDomains {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
            //retrieve plan
            $profileModel = new UserProfile();
            $profile = $profileModel->getWhereOne('user_id', $userId);
            
            if(!$profile){
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('plan.missing')];
                return $result;
            }

            $planId = $profile['plan_id'];
            //retrieve build
            $planModel = new Plan();
            $plan = $planModel->get($planId);

            if(!$plan){
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('plan.invalid')];
                return $result;
            }

            $min = $plan['min'];
            $max = $plan['max'];
            return [
                'error' => false,
                'code' => 200,
                'min' => $min,
                'max' => $max
            ];
    }
}