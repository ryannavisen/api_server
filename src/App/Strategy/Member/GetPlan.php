<?php namespace App\Strategy\Member;

use App\Repository\UserProfile;
use App\Repository\Plan;
use Symfony\Component\Validator\Constraints as Assert;

class GetPlan {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $result = [];
        $profileModel = new UserProfile();
        $profile = $profileModel->getWhereOne('user_id', $userId);
        if(!$profile){
            $result['error'] = true;
            $result['code'] = 403;
            $result['message']['plan'] = [$app['translator']->trans('plan.missing')];
            return $result;
        }
        
        $planId = $profile['plan_id'];
        //retrieve build
        $planModel = new Plan();
        $plan = $planModel->get($planId);

        if(!$plan){
            $result['error'] = true;
            $result['code'] = 403;
            $result['message']['domain'] = [$app['translator']->trans('plan.invalid')];
            return $result;
        }
        return $plan;
    }
}