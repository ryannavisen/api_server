<?php namespace App\Strategy\Member;

use App\Repository\Domains;
use App\Repository\UserProfile;
use App\Repository\Plan;
use App\Repository\Codebase;
use Symfony\Component\Validator\Constraints as Assert;

class AddDomain {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'domain' => isset($parameter['domain']) ? $parameter['domain'] : ''
        ];

        $constraint = new Assert\Collection(array(
            'domain' => [
                new Assert\NotBlank(array('message' => 'domain.short'))
            ]
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            //retrieve plan
            $profileModel = new UserProfile();
            $profile = $profileModel->getWhereOne('user_id', $userId);
            
            if(!$profile){
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('plan.missing')];
                return $result;
            }

            $planId = $profile['plan_id'];
            //retrieve build
            $planModel = new Plan();
            $plan = $planModel->get($planId);

            if(!$plan){
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('plan.invalid')];
                return $result;
            }

            $min = $plan['min'];
            $max = $plan['max'];

            $model = new Domains();
            $usedDomains = $model->countDomains($userId);

            if($usedDomains > $max) {
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('plan.exceed')];
                return $result;                
            }

            $codebaseModel = new Codebase();

            if($usedDomains > $max) {
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['domain'] = [$app['translator']->trans('codebase.issue')];
                return $result;                
            }            

            $codebase = $codebaseModel->getLatestCode();

            $data['user_id'] = $userId;
            $data['domain_hash'] = sha1($data['domain']);
            $data['prebuild'] = $codebase['prebuild'];
            $data['afterbuild'] = $codebase['afterbuild'];
            $data['version'] = $codebase['version'];
            $data['status'] = 1;
            $data['created_at'] = date('Y-m-d h:m:s');
            $data['updated_at'] = date('Y-m-d h:m:s');
            $result = $model->insert($data);
        }

        return $result;
    }
}