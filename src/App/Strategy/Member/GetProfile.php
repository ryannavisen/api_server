<?php namespace App\Strategy\Member;

use App\Repository\User;
use Symfony\Component\Validator\Constraints as Assert;

class GetProfile {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [];
        $model = new User();
        $result = $model->get($userId);
        $result = $model->transform($result);
        return $result;
    }
}