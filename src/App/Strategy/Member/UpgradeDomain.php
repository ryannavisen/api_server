<?php namespace App\Strategy\Member;

use App\Repository\Domains;
use App\Repository\UserProfile;
use App\Repository\Plan;
use App\Repository\Codebase;
use Symfony\Component\Validator\Constraints as Assert;

class UpgradeDomain {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'id' => isset($parameter['id']) ? (int)$parameter['id'] : ''
        ];

        $constraint = new Assert\Collection(array(
            'id' => [
                new Assert\NotBlank(array('message' => 'id.missing'))
            ]
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $domainModel = new Domains();
            $domain = $domainModel->get($data['id']);
            if(!$domain){
                $result['error'] = true;
                $result['code'] = 403;
                $result['message']['id'] = [$app['translator']->trans('domain.missing')];
                return $result;
            }

            $codebaseModel = new Codebase();
            $codebase = $codebaseModel->getLatestCode();

            $domain['prebuild'] = $codebase['prebuild'];
            $domain['afterbuild'] = $codebase['afterbuild'];
            $domain['version'] = $codebase['version'];
            $data['updated_at'] = date('Y-m-d h:m:s');
            $result = $domainModel->update($data['id'], $domain);
        }

        return $result;
    }
}