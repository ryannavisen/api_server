<?php namespace App\Strategy\Member;

use App\Repository\Domains;

class ListDomains {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $model = new Domains();
        return $model->getWhere('user_id', $userId);
    }
}