<?php namespace App\Strategy\Admin;

use App\Repository\Settings;

class IsMaintanence {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $setting = new Settings();
        $result = $setting->getWhere ('name', 'maintenance');
        $app['monolog']->debug('fun stuff');
        return [
            'operation' => ($result['value'] == '1') ? true : false
        ];
    }
}