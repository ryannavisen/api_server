<?php namespace App\Strategy\Admin;

use App\Repository\User;
use App\Repository\UserProfile;
use Symfony\Component\Validator\Constraints as Assert;

class AddUsers {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'email' => isset($parameter['email']) ? $parameter['email'] : '',
            'username' => isset($parameter['username']) ? $parameter['username'] : $parameter['email'],
            'password' => isset($parameter['password']) ? $parameter['password'] : '',
            'organization_id' => $app['organization_id'],
            'profile_id' => 0,
            'profile_image' => isset($parameter['profile_image']) ? $parameter['profile_image'] : '',
            'first_name' => isset($parameter['first_name']) ? $parameter['first_name'] : '',
            'last_name' => isset($parameter['last_name']) ? $parameter['last_name'] : '',
            'created' => date('Y-m-d h:m:s'),
            'updated' => date('Y-m-d h:m:s'),
            'role_id' => 2,
            'status' => 0,
            'token' => ''
        ];

        $constraint = new Assert\Collection(array(
            'email' => [
                new Assert\NotBlank(array('message' => 'email.short')),
                new Assert\Email(array(
                    'message' => 'email.invalid'
                ))
            ],
            'password' => [
                new Assert\NotBlank(array('message' => 'password.short')),
                new Assert\Length(array('min' => 8, 'max' => 12, 'minMessage' => 'password.short', 'maxMessage' => 'password.long')),
                new Assert\Regex(array('pattern' => "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/", 'message' => 'password.invalid'))
            ],            
            'profile_image' => [
                new Assert\NotBlank(array('message' => 'image.short')),
                new Assert\Length(array('min' => 1, 'max' => 255, 'minMessage' => 'image.short', 'maxMessage' => 'image.long'))
            ],
            'first_name' => [
                new Assert\NotBlank(array('message' => 'first_name.short')),
                new Assert\Length(array('min' => 1, 'max' => 30, 'minMessage' => 'first_name.short', 'maxMessage' => 'first_name.long'))
            ],
            'last_name' => [
                new Assert\NotBlank(array('message' => 'last_name.short')),
                new Assert\Length(array('min' => 1, 'max' => 30, 'minMessage' => 'last_name.short', 'maxMessage' => 'last_name.long'))
            ],                        
            'role_id' => [
                new Assert\Range(array(
                    'min'        => 1,
                    'minMessage' => 'role.invalid'
                ))
            ],                                   
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $data['organization_id'] = $organizationId;
            $model = new User();
            $profileModel = new UserProfile();
            $user = $model->insert($data);
            $defaultProfile = UserProfile::defaultProfile([
                'user_id' => $user['id']
            ]);
            $profile = $profileModel->insert($defaultProfile);
            $newUser = $model->update($user['id'], [
                'user_id' => $profile['id']
            ]);
            $result = $model->transform($user);
        }

        return $result;
    }
}