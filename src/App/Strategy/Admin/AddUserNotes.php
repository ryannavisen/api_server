<?php namespace App\Strategy\Admin;

use App\Repository\UserNote;
use Symfony\Component\Validator\Constraints as Assert;

class AddUserNotes {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'description' => isset($parameter['description']) ? $parameter['description'] : '',
            'user_id' => isset($parameter['user_id']) ? $parameter['user_id'] : 0
        ];
        $constraint = new Assert\Collection(array(
            'description' => [
                new Assert\NotBlank(array('message' => 'description.short'))
            ],
            'user_id' => [
                new Assert\NotBlank(array('message' => 'user_id.missing'))
            ]                                    
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $model = new UserNote();
            $data['created_at'] = date('Y-m-d h:m:s');
            $result = $model->insert($data);
        }

        return $result;  
    }
}