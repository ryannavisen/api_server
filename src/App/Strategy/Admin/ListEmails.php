<?php namespace App\Strategy\Admin;

use App\Repository\EmailTemplate;

class ListEmails {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $model = new EmailTemplate();
        return $model->getAll();
    }
}