<?php namespace App\Strategy\Admin;

use App\Repository\EmailTemplate;
use Symfony\Component\Validator\Constraints as Assert;

class AddEmails {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'slug' => isset($parameter['slug']) ? $parameter['slug'] : '',
            'from' => isset($parameter['from']) ? $parameter['from'] : '',
            'subject' => isset($parameter['subject']) ? $parameter['subject'] : '',
            'html' => isset($parameter['html']) ? $parameter['html'] : '',
            'text' => isset($parameter['text']) ? $parameter['text'] : ''
        ];

        $constraint = new Assert\Collection(array(
            'slug' => [
                new Assert\NotBlank(array('message' => 'slug.short')),
                new Assert\Length(array('min' => 1, 'max' => 100, 'minMessage' => 'slug.short', 'maxMessage' => 'slug.long'))
            ],
            'from' => [
                new Assert\NotBlank(array('message' => 'from.short')),
                new Assert\Length(array('min' => 1, 'max' => 255, 'minMessage' => 'from.short', 'maxMessage' => 'from.long'))
            ],
            'subject' => [
                new Assert\NotBlank(array('message' => 'subject.short')),
                new Assert\Length(array('min' => 1, 'max' => 255, 'minMessage' => 'subject.short', 'maxMessage' => 'subject.long'))
            ],
            'html' => [
                new Assert\NotBlank(array('message' => 'html.short'))
            ],
            'text' => []                                    
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $model = new EmailTemplate();
            $data['organization_id'] = $organizationId;
            $result = $model->insert($data);
        }

        return $result;
    }
}