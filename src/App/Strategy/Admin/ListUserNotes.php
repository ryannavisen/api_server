<?php namespace App\Strategy\Admin;

use App\Repository\UserNote;

class ListUserNotes {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'page' => isset($parameter['page']) ? $parameter['page'] : 1,
            'item_per_page' => isset($parameter['item_per_page']) ? $parameter['item_per_page'] : 10,
            'order' => isset($parameter['order']) ? $parameter['order'] : 'ASC'
        ];


        $model = new UserNote();
        return $model->paginate($data['page'], $data['item_per_page'], $data['order']);
    }
}