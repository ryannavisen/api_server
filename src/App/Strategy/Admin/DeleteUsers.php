<?php namespace App\Strategy\Admin;

use App\Repository\Settings;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteUsers {

    public static function handler ($userId, $organizationId, $app, $parameter = [], $response = [])
    {
        $data = [
            'id' => isset($parameter['id']) ? $parameter['id'] : 0
        ];
        
        $constraint = new Assert\Collection(array(
            'id' => [
                new Assert\Range(array(
                    'min'        => 1,
                    'minMessage' => 'id.invalid'
                ))
            ]                                   
        ));

        $errors = $app['validator']->validate($data, $constraint);
        $result = [
            'error' => false,
            'code' => 200
        ];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['code'] = 403;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $model = new User();
            $model->update($data['id'], [
                'status' => -1
            ]);
        }

        return $result;  
    }
}