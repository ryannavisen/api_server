<?php
namespace App\Service;

class EmailService implements EmailServiceInterface {

    private $_repo;
    private $_adapter;

    public function __construct ($emailRepository, $emailAdapter)
    {
        $this->_repo = $emailRepository;
        $this->_adapter = $emailAdapter;
    }

    public function send ($from, $to, $subject, $html, $sendNow = false)
    {
        if ($sendNow) {
            $this->_adapter->send($from, $to, $subject, $html);
            return true;
        } else {
            return $this->_repo->insert([
              'to' => $to,
              'from' => $from,
              'subject' => $subject,
              'html' => $html,
              'created' => date('Y-M-D hh:mm:ss'),
              'status' => 0
            ]);
        }

    }
}