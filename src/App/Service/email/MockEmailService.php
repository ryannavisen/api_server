<?php
namespace App\Service;

class MockEmailService implements EmailServiceInterface {

    private $_logger;
    private $_repo;

    public function __construct ($logger, $emailRepository)
    {
        $this->_logger = $logger;
        $this->_repo = $emailRepository;
    }

    public function send ($from, $to, $subject, $html, $sendNow = true)
    {
        if ($sendNow) {
            $this->_logger->info("FROM $from \n");
            $this->_logger->info("TO $to \n");
            $this->_logger->info("SUBJECT $subject \n");
            $this->_logger->info("HTML $html \n");
            return true;
        } else {
            return $this->_repo->insert([
              'to' => $to,
              'from' => $from,
              'subject' => $subject,
              'html' => $html,
              'created' => date('Y-M-D hh:mm:ss'),
              'status' => 0
            ]);
        }

    }
}