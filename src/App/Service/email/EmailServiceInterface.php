<?php
namespace App\Service;

interface EmailServiceInterface {
    public function send ($from, $to, $subject, $html, $sendNow);
}