<?php namespace App\Service;

class RequestStrategyService {
    public static function parseParameter ($parameters)
    {
        if (!$parameters) {
            return false;
        }

        return json_decode($parameters, true);
    }

    public static function parseResponse ($responses)
    {
        if(!is_array($responses)) {
            return [];
        }

        return $responses;
    }
}