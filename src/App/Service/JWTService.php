<?php
namespace App\Service;

use \Firebase\JWT\JWT;
use App\Exceptions\TokenException;
use \DomainException;
use \InvalidArgumentException;
use \UnexpectedValueException;

class JWTService {
    protected $_expireAt;
    protected $_key;
    protected $_base_url;
    protected $_server_name;
    protected $_alg;

    const ACCESS_TOKEN = 1;

    public function  __construct($app)
    {
        $this->_expireAt = $app['jwt.options']['expireAt'];
        $this->_key = $app['jwt.options']['key'];
        $this->_base_url = $app['base_url'];
        $this->_server_name = $app['server_name'];
        $this->_alg = $app['jwt.options']['alg'];
    }

    public function createToken($payload) 
    {
        $tokenId    = base64_encode(mcrypt_create_iv(32));
        $issuedAt   = time();
        // $notBefore  = $issuedAt + 10;
        $notBefore  = $issuedAt;
        $expire = $notBefore + $this->_expireAt;
        $token      = [
            'iat'  => $issuedAt,           // Issued at: time when the token was generated
            'jti'  => $tokenId,            // Json Token Id: an unique identifier for the token
            'iss'  => $this->_server_name, // Issuer
            'nbf'  => $notBefore,          // Not before
            'exp'  => $expire,             // Expire
            'data' => $payload        
        ];

        return [
            'token' => JWT::encode($token, $this->_key, $this->_alg),
            'expire_at' => $expire
        ];
    }

    public function verifyToken($token) 
    {
        try {
            return (array)JWT::decode($token, $this->_key, [$this->_alg]);
        } catch (\Exception $e) {
            if ($e instanceof \InvalidArgumentException) {
                error_log('InvalidArgumentException');
                throw new TokenException('Invalid token provided', null, []);
            }
            if ($e instanceof \DomainException) {
                error_log('DomainException');
                throw new TokenException('Invalid token provided', null, []);
            }
            if ($e instanceof \UnexpectedValueException) {
                error_log('UnexpectedValueException');
                error_log(print_r($e,true));
                throw new TokenException('Invalid token format provided', null, []);
            }
            if ($e instanceof Firebase\JWT\ExpiredException) {
                error_log('ExpireException');
                throw new TokenException('Expired token provided', null, []);
            }
            if ($e instanceof Firebase\JWT\BeforeValidException) {
                error_log('BeforeValidException');
                throw new TokenException('Future token provided', null, []);
            }
        }
    }

    public function getExpireAt ()
    {
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;
        $expire = $notBefore + $this->_expireAt;
        return $expire;
    }

}