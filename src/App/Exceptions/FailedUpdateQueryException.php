<?php
namespace App\Exceptions;

class FailedUpdateQueryException extends \RuntimeException
{
    private $statusCode;
    private $headers;

    public function __construct($message = null, \Exception $previous = null, array $headers = array(), $code = 0)
    {
        $this->statusCode = 403;
        $this->headers = $headers;

        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set response headers.
     *
     * @param array $headers Response headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }
}
