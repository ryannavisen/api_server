<?php

namespace App\Adapter;

interface DbInterface {
    public function get ($id);
    public function getAll ();
    public function paginate ($page, $limit, $order);
    public function update ($id, $data);
    public function insert ($field);
    public function delete ($id);
    public function getWhere ($field, $value);
    public function transform ($data);
}