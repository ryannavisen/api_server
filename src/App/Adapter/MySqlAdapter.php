<?php
namespace App\Adapter;

use App\Exceptions\NotFoundException;
use App\Exceptions\FailedUpdateQueryException;
use App\Exceptions\FailedDeleteQueryException;
use App\Adapter\DbInterface;

use RedBeanPHP\Facade as R;

class MySqlAdapter implements DbInterface {
    protected $_table;

    public function __construct ($table)
    {
        $this->_table = $table;
    }

    public function get ($id)
    {
        // $sql = "SELECT * FROM $this->_table WHERE id = ?";
        $result = R::findOne($this->_table, ' id = ? ', [ $id ] );

        // if ($result === NULL)
        // {
        //     throw new NotFoundException($this->_table . ' cannot be found', null, []);
        // }

        return $result;
    } 

    public function getAll ()
    {
        // $sql = "SELECT * FROM $this->_table ";
        $result = R::findAll($this->_table);
        // if (!$result)
        // {
        //     throw new NotFoundException($this->_table . ' cannot be found', null, []);
        // }

        return array_values($result);
    }

    public function paginate ($page = 1, $limit = 10, $order = 'ASC')
    {
        $total = R::count( $this->_table);
        $needles=R::count( $this->_table );
        $numPages = ($total > 1) ? ceil($total / $limit) : 1;
        $result = array_values(R::findAll( $this->_table ,"ORDER BY id $order LIMIT ". (($page - 1) * $limit) . ', ' . $limit));
        $id = 0;
        if (count($result) > 0) {
            $last = array_slice($result, -1)[0];
            $id = $last['id'];
        }
        return [
            'type' => 'Page',
            'previous_page' => ($page - 1 < 1) ? 1 : ($page - 1),
            'next_page' => ($page + 1 > $numPages ) ? $numPages : ($page + 1),
            'num_page' => $numPages,
            'num_items' => $total,
            'id' => (int) $id,
            'items' => $result
        ];
    }

    public function update ($id, $fields = [])
    {
        $model = $this->get($id);

        if (!$model['id']) {
            throw new NotFoundException($this->_table . ' cannot be found', null, []);
        }

        $fieldNames = [];
        $fieldValues = [];
        foreach ($fields as $key => $value) {
            $fieldNames[] = $key;
            $fieldValues[] = $value;
        }

        $numFields = count($fieldNames);

        if ($numFields < 1) {
            return 1;
        } 
        
        foreach ($fields as $key => $field) {
            $model[$key] = $field;
        }

        $result = R::store( $model );
        
        if (!$result)
        {
            throw new FailedUpdateQueryException($this->_table . ' updating failed', null, []);
        }

        return $result;
    }

    public function insert ($fields = [])
    {
        $model = R::dispense( $this->_table);
        foreach ($fields as $key => $field) {
            $model[$key] = $field;
        }
        
        $id = R::store( $model );

        if (!$id)
        {
            throw new FailedInsertQueryException($this->_table . ' creation failed', null, []);
        }
        
        return $this->get($id);
    }

    public function delete ($id)
    {
        $model = $this->get($id);
        R::trash( $model );
        return true;
    }             

    public function getWhere ($field, $value)
    {
        // $sql = "SELECT * FROM $this->_table WHERE $field = ?";
        $result  = array_values(R::find( $this->_table, " $field = ? ", [ $value ]));
        return $result;
    }

    public function getWhereOne ($field, $value)
    {
        // $sql = "SELECT * FROM $this->_table WHERE $field = ?";
        $result  = R::findOne( $this->_table, " $field = ? ", [ $value ] );
        return $result;
    }

    public function transform ($data)
    {
        return $data;
    }

    public function query ($sql)
    {
        return R::getAll($sql);
    }
    
    public function getColumns ()
    {
        return R::inspect( $this->_table );
    }

    public static function getAllTables ()
    {
        return R::inspect();
    }

    public function transaction ($transactionFunction)
    {
        R::begin();
        try{
            $transactionFunction();
            R::commit();
        }
        catch( Exception $e ) {
            R::rollback();
            throw new FailedInsertQueryException($this->_table . ' transaction failed', null, []);
        }        
    }

    public function duplicate ($model)
    {
        return R::duplicate( $model );
    }
}