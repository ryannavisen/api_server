<?php
namespace App\Lambda;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\User;
use App\Repository\Token;

class Login {

    public function index (Request $request, Application $app)
    {
        $data = [
            'email' => $request->request->get('email', ''),
            'password' => $request->request->get('password', '')
        ];

        $lang = $request->query->get('lang', 'en');
        //new Assert\Language()
        $app['translator']->setLocale($lang);

        $constraint = new Assert\Collection(array(
            'email' => [
                new Assert\NotBlank(array('message' => 'email.short')),
                new Assert\Email(array(
                    'message' => 'email.invalid'
                ))
            ],
            'password' => [
                new Assert\NotBlank(array('message' => 'password.short')),
                new Assert\Length(array('min' => 8, 'max' => 12, 'minMessage' => 'password.short', 'maxMessage' => 'password.long')),
                new Assert\Regex(array('pattern' => "/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/", 'message' => 'password.invalid'))
            ]
        ));
        $errors = $app['validator']->validate($data, $constraint);
        $result = [];

        if (count($errors) > 0) {
            $result['error'] = true;
            $result['message'] = [];
            foreach ($errors as $error) {
                $field = substr($error->getPropertyPath(), 1, strlen($error->getPropertyPath()) - 2);
                if (array_key_exists($field)) {
                    $result['message'][$field] = [$error->getMessage()];
                } else {
                    $result['message'][$field][] = $error->getMessage();
                }
            }
        } else {
            $userModel = new User();
            $result = $userModel->authenticateUser($data['email'], $data['password'], $app);

            if (!$result['error']) {
                $tokenModel = new Token($app['jwt_service']);
                $token = $tokenModel->createAccessToken($result['model']);
                unset($result['model']);
                $result = $tokenModel->transformAccessToken($token);
            }
        }
        return $app->json($result, 200);
    }    
}
