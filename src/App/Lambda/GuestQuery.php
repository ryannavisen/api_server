<?php
namespace App\Lambda;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use App\Service\RequestStrategyService;

use App\Strategy\Guest\AddPayment;
use App\Strategy\Guest\AddSubscription;
class GuestQuery {

	public function index (Request $request, Application $app) {
		$action = $request->query->get('action', '');
		$parameter = $request->query->get('parameter', '');
		$lang = $request->query->get('lang', 'en');
		$response = $request->query->get('response', []);
		////new Assert\Language()
		$app['translator']->setLocale($lang);
		$userId = $app['user_id'];
		$organizationId = $app['organization_id'];
		$organizationId = $app['role_id'];
		$filteredParameters = RequestStrategyService::parseParameter($parameter);
		$filteredResponses = RequestStrategyService::parseResponse($response);

		$defaultErrorMessage = [
			'message' => $app['translator']->trans('Invalid API Request')
		];

		if (!$userId || !$organizationId || !$action || !$parameter || !$response) {
			return $app->json([
				'message' => $defaultErrorMessage
			], 404);
		}
    
    	error_log('Action: ' . $action);
		error_log('Parameter: ' . print_r($filteredParameters, true));
		error_log('Response: ' . print_r($filteredResponses, true));

		try {
			$className = 'App\\Strategy\\Guest\\' . $action;
			
			if (class_exists($className)) {
				$result = $className::handler($userId, $organizationId, $app, $filteredParameters, $filteredResponses);
				return $app->json($result, 200);
			} else {
				return $app->json($defaultErrorMessage, 404);
			}
			
		} catch (\Exception $e) {
			var_dump(get_class($e));
			var_dump($e); exit;
		}
	}

	public function methods (Request $request, Application $app) 
	{
		$ignoreFiles = ['.DS_Store', '.', '..'];
		$result = [];
		if ($handle = opendir(__DIR__ . '/../strategy/guest')) {
		
			while (false !== ($entry = readdir($handle))) {
		
				if (!in_array($entry, $ignoreFiles)) {
					$result[] = str_replace('.php', '', $entry);
				}
			}
			closedir($handle);
		}

		return $app->json($result, 200);
	}
}
