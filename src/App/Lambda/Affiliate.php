<?php
namespace App\Lambda;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;

class Affiliate {

    public function index (Request $request, Application $app)
    {
        $data = [
            'code' => $request->request->get('code', 'ryan@manaknightdigital.com')
        ];
        return $app->json([], 200);
    }    
}
