<?php
namespace App\Lambda;

use Swagger\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use App\Repository\Domains;

class BuildPackage {

    public function index (Request $request, Application $app)
    {
        $id  = (int)$request->get('id', 0);
        if (!$id) {
            return $app->json([
                'error' => true,
                'message' => $app['translator']->trans('id.missing')
			], 404);
        }

        $userId = $app['user_id'];

        $domainModel = new Domains();
        $domain = $domainModel->get($id);

        if(!$domain){
            $result['error'] = true;
            $result['code'] = 403;
            $result['message']['id'] = [$app['translator']->trans('domain.missing')];
            return $result;
        }

        if($domain['user_id'] != $userId){
            $result['error'] = true;
            $result['code'] = 403;
            $result['message']['id'] = [$app['translator']->trans('domain.bad_credentials')];
            return $result;
        }        

        $payload = $domain['prebuild'] . $domain['domain_hash'] . $domain['afterbuild'];
        $filename = __DIR__ . '/../../../web/uploads/' . $domain['domain_hash'];
        // return $app->json([$payload], 200);
        file_put_contents($filename, $payload);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"" . 'simplepresenter.js' . "\""); 
        readfile($filename);
        exit();
        
    }    
}
