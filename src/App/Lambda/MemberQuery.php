<?php
namespace App\Lambda;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use App\Service\RequestStrategyService;

use App\Strategy\Member\ListDomains;
use App\Strategy\Member\AddDomain;
use App\Strategy\Member\AllowDomains;

class MemberQuery {

	public function index (Request $request, Application $app) {
		$action = $request->request->get('action', '');
		$parameter = $request->request->get('parameter', '');
		$lang = $request->request->get('lang', 'en');
		$response = $request->request->get('response', []);
		////new Assert\Language()
		$app['translator']->setLocale($lang);
		$userId = $app['user_id'];
		$organizationId = 1;
		$roleId = $app['role_id'];
		$filteredParameters = RequestStrategyService::parseParameter($parameter);
		$filteredResponses = RequestStrategyService::parseResponse($response);

		$defaultErrorMessage = [
			'message' => $app['translator']->trans('Invalid API Request')
		];

		if (!$userId || !$organizationId || !$action || strlen($parameter) < 1) {
			return $app->json([
				'message' => $defaultErrorMessage
			], 404);
		}
    
    	error_log('Action: ' . $action);
		error_log('Parameter: ' . print_r($filteredParameters, true));
		error_log('Response: ' . print_r($filteredResponses, true));

		try {
			$className = 'App\\Strategy\\Member\\' . $action;
			
			if (class_exists($className)) {
				$result = $className::handler($userId, $organizationId, $app, $filteredParameters, $filteredResponses);
				return $app->json($result, 200);
			} else {
				return $app->json($defaultErrorMessage, 404);
			}
			
		} catch (\Exception $e) {
			error_log($e);
			return $app->json([
				'message' => $app['translator']->trans('error.default')
			], 403);
		}
	}

	public function methods (Request $request, Application $app) 
	{
		$ignoreFiles = ['.DS_Store', '.', '..'];
		$result = [];
		if ($handle = opendir(__DIR__ . '/../Strategy/Member')) {
		
			while (false !== ($entry = readdir($handle))) {
		
				if (!in_array($entry, $ignoreFiles)) {
					$result[] = str_replace('.php', '', $entry);
				}
			}
			closedir($handle);
		}

		return $app->json($result, 200);
	}	
}
