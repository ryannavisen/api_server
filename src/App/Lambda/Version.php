<?php
namespace App\Lambda;

use Swagger\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

class Version {

    public function index (Request $request, Application $app)
    {
        $jsonString = file_get_contents(__DIR__ . '/../../../composer.json');
        $json = json_decode($jsonString, true);
        return $app->json([
            'version' => $json['version']
        ]);
    }    
}
