<?php
namespace App\Lambda;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use App\Service\RequestStrategyService;

use App\Strategy\Admin\IsMaintanence;
use App\Strategy\Admin\AddEmails;
use App\Strategy\Admin\AddPermissions;
use App\Strategy\Admin\AddRolePermissions;
use App\Strategy\Admin\AddRoles;
use App\Strategy\Admin\AddUserNotes;
use App\Strategy\Admin\AddUsers;
use App\Strategy\Admin\AddAdminUsers;
use App\Strategy\Admin\DeletePermissions;
use App\Strategy\Admin\DeleteRolePermissions;
use App\Strategy\Admin\DeleteRoles;
use App\Strategy\Admin\DeleteUsers;
use App\Strategy\Admin\EditEmails;
use App\Strategy\Admin\EditPermissions;
use App\Strategy\Admin\EditProfile;
use App\Strategy\Admin\EditRolePermissions;
use App\Strategy\Admin\EditRoles;
use App\Strategy\Admin\EditToken;
use App\Strategy\Admin\EditUsers;
use App\Strategy\Admin\ListAdminLog;
use App\Strategy\Admin\ListLoginLog;
use App\Strategy\Admin\ListPermissions;
use App\Strategy\Admin\ListRolePermissions;
use App\Strategy\Admin\ListRoles;
use App\Strategy\Admin\ListTokens;
use App\Strategy\Admin\ListUserLog;
use App\Strategy\Admin\ListUserNotes;
use App\Strategy\Admin\ListUsers;

class AdminQuery {

	public function index (Request $request, Application $app)
	{
		$action = $request->query->get('action', '');
		$parameter = $request->query->get('parameter', '');
		$lang = $request->query->get('lang', 'en');
		$response = $request->query->get('response', []);
		////new Assert\Language()
		$app['translator']->setLocale($lang);
		$userId = $app['user_id'];
		$organizationId = $app['organization_id'];
		$organizationId = $app['role_id'];
		$filteredParameters = RequestStrategyService::parseParameter($parameter);
		$filteredResponses = RequestStrategyService::parseResponse($response);

		$defaultErrorMessage = [
			'message' => $app['translator']->trans('Invalid API Request')
		];

		if (!$userId || !$organizationId || !$action || !$parameter || !$response) {
			return $app->json([
				'message' => $defaultErrorMessage
			], 404);
		}
    
    	error_log('Action: ' . $action);
		error_log('Parameter: ' . print_r($filteredParameters, true));
		error_log('Response: ' . print_r($filteredResponses, true));

		try {
			$className = 'App\\Strategy\\Admin\\' . $action;
			
			if (class_exists($className)) {
				$result = $className::handler($userId, $organizationId, $app, $filteredParameters, $filteredResponses);
				return $app->json($result, 200);
			} else {
				return $app->json($defaultErrorMessage, 404);
			}
			
		} catch (\Exception $e) {
			// var_dump(get_class($e));
			// var_dump($e); exit;
			print_r($e);
		}
	}

	public function methods (Request $request, Application $app) 
	{
		$ignoreFiles = ['.DS_Store', '.', '..'];
		$result = [];
		if ($handle = opendir(__DIR__ . '/../strategy/admin')) {
		
			while (false !== ($entry = readdir($handle))) {
		
				if (!in_array($entry, $ignoreFiles)) {
					$result[] = str_replace('.php', '', $entry);
				}
			}
			closedir($handle);
		}

		return $app->json($result, 200);
	}
}
