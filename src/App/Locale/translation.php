<?php

$translation = array(
    'messages' => array(
        'en' => array(
            'Invalid API Request' => 'Invalid API Request',
            'hello'     => 'Hello %name%',
            'goodbye'   => 'Goodbye %name%',
            'error.default' => 'Something went wrong.',
            'plan.missing' => 'Plan is not set',
            'plan.invalid' => 'Plan Misconfigured',
            'plan.exceed' => 'You have exceeded your plan. Please upgrade to add more domains',
            'codebase.issue' => 'The codebase is having issues. Please contact an admin',
            'domain.missing' => 'Domain Not Found',
            'id.missing' => 'Missing ID',
            'domain.bad_credentials' => 'You don\'t own this domain'
        )
    ),
    'validators' => array(
        'en' => [
            'This value should not be blank.' => 'This value is required.',
            'email.not_blank' => 'Email is required.',
            'password.short' => 'Your Password is too short. You need at least 8 characters.',
            'password.invalid' => 'Your password is too weak. You need at least 1 Uppercase, 1 Number.',
            'password.long' => 'Your Password is too long. You need at most 12 characters.',
            'password.incorrect' => 'Your Password does not match.',
            'email.not_found' => 'Your email cannot be found',
            'email.invalid' => 'Your email is invalid',
            'slug.short' => 'Slug is too short',
            'slug.long' => 'Slug is longer than 100 letters.',
            'from.short' => 'From Address is too short',
            'from.long' => 'From Address is longer than 100 letters.',
            'subject.short' => 'Subject is too short',
            'subject.long' => 'Subject is longer than 255 letters.',
            'html.short' => 'HTML Template is required',
            'description.short' => 'Description is too short',
            'image.short' => 'Image URL is too short',
            'image.long' => 'Image URL is longer than 255 letters.',
            'name.short' => 'Name is too short',
            'first_name.short' => 'First Name is too short',
            'first_name.long' => 'First Name is longer than 30 letters.',
            'last_name.short' => 'Last Name is too short',
            'last_name.long' => 'Last Name is longer than 30 letters.',
            'user_id.missing' => 'User is missing',
            'status_basic.invalid' => 'Status must be Active or Inactive',
            'descriptionrolepermission.long' => 'Description must be shorter than 150 letters.',
            'namerolepermission.long' => 'Name must be shorter than 100 letters.',
            'role.invalid' => 'Role is not valid',
            'permission.invalid' => 'Permission is not valid',
            'id.missing' => 'Missing ID',
            'status.short' => 'Status missing',
            'domain.short' => 'Status missing'
        ]
    ),
);

return $translation;