<?php
namespace App\Utility;

class PasswordGenerator {
    public static function make ($plainText)
    {
        $cost = 11;
        return password_hash('~' . $plainText . '!', PASSWORD_BCRYPT, array('cost' => $cost));
    }

    public static function verify ($plaintext, $cipher)
    {
        return password_verify('~' . $plaintext . '!', $cipher);
    }    
}