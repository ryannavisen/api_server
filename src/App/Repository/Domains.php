<?php namespace App\Repository;

use App\Repository\Base;

class Domains extends Base {
    protected $_table = 'domains';

    public function __construct ()
    {
        parent::__construct();
    }

    public function countDomains ($userId)
    {
        $result = $this->query('SELECT count(*) as num FROM domains WHERE user_id=' . $userId);
        // error_log(print_r($result));
        return $result[0]['num'];
    }
}