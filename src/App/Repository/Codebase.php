<?php namespace App\Repository;

use App\Repository\Base;

class Codebase extends Base {
    protected $_table = 'codebase';

    public function __construct ()
    {
        parent::__construct();
    }

    public function getLatestCode ()
    {
        $result = $this->query('SELECT * FROM codebase WHERE 1 ORDER BY id DESC LIMIT 1');
        // error_log(print_r($result));
        return $result[0];        
    }

}