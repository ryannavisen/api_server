<?php namespace App\Repository;

use App\Repository\Base;
use App\Exceptions\NotFoundException;
use App\Service\JWTService;

class Token extends Base {
    protected $_table = 'token';
    protected $_service;

    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct ($jwtService)
    {
        parent::__construct();
        $this->_service = $jwtService;
    }

    public function tokenExist ($token)
    {
        try {
            $model = $this->getWhereOne('token_hash', sha1($token));
            return $model['id'];
        } catch (NotFoundException $e) {
            return false;
        }
    }

    public function createAccessToken($user)
    {
        $jwtPayload = [
            'user_id' => $user['id'],
            'role' => $user['role_id']
        ];

        $token = $this->_service->createToken($jwtPayload);
        
        $data = [
            'user_id' => $user['id'],
            'action' => JWTService::ACCESS_TOKEN,
            'data' => json_encode($jwtPayload),
            'token' => $token['token'],
            'token_hash' => sha1($token['token']),
            'expire_at' => $token['expire_at'],
            'created' => date('Y-m-d h:m:s'),
            'status' => self::ACTIVE
        ];

        return $this->insert($data);
    }    

    public function transformAccessToken ($data)
    {
        return [
            'error' => false,
            'user_id' => (int)$data['user_id'],
            'access_token' => $data['token'],
            'expire_at' => $data['expire_at']
        ];
    }
}