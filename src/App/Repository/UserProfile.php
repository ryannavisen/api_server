<?php namespace App\Repository;

use App\Repository\Base;

class UserProfile extends Base {
    protected $_table = 'userprofile';

    public function __construct ()
    {
        parent::__construct();
    }

    public static function defaultProfile ($overridingFields = [])
    {
        $profile = [
            'address' => '',
            'city_id' => 0,
            'state_id' => 0,
            'country_id' => 0,
            'postal_code' => '',
            'phone' => 0,
            'fax' => 0,
            'gender' => 'N',
            'age' => 0,
            'birth_date' => date('Y-m-d'),
            'extension' => 0,
            'device_id' => 0,
            'device_type' => 'N',
            'latitude' => 0,
            'longitude' => 0,
            'security_question_1' => '',
            'security_question_2' => '',
            'security_question_3' => '',
            'security_answer_1' => '',
            'security_answer_2' => '',
            'security_answer_3' => '',
            'description' => '',
            'notes' => '',
            'position' => '',
            'platform' => 'N',
            'user_id' => $overridingFields['user_id'],
            'rate' => 0,
            'balance' => 0,
            'last_login' => date('Y-m-d h:m:s'),
            'last_ip' => '',
            'terms_and_condition' => 0,
            'two_factor_enabled' => 0,
            'is_approved' => 0,
            'mobile_pin' => 0,
            'desktop_pin' => 0,
            'affilate_id' => 0,
            'subscription_id' => 0,
            'plan_id' => 0
        ];

        foreach ($overridingFields as $key => $value) {
            $profile[$key] = $value;
        }

        return $profile;
    }    
}