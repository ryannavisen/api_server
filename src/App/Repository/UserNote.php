<?php namespace App\Repository;

use App\Repository\Base;

class UserNote extends Base {
    protected $_table = 'usernote';

    public function __construct ()
    {
        parent::__construct();
    }

}