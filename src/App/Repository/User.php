<?php namespace App\Repository;

use App\Repository\Base;
use App\Utility\PasswordGenerator;
use App\Exceptions\NotFoundException;

class User extends Base {
    protected $_table = 'user';

    public function __construct ()
    {
        parent::__construct();
    }

    public function authenticateUser ($email, $password, $app)
    {
        try {
            $model = $this->getWhereOne('email', $email);
            $result = PasswordGenerator::verify($password, $model['password']);

            if (!$result) {
                return [
                    'error' => true,
                    'message' => $app['translator']->trans('password.incorrect')
                ];
            }
            return [
                'error' => false,
                'model' => $model
            ];
        } catch (NotFoundException $e) {
            return [
                'error' => true,
                'message' => $app['translator']->trans('email.not_found')
            ];
        }
    }

    public function emailExist ($email)
    {
        try {
            $model = $this->getWhereOne('email', $email);
            return $model['id'];
        } catch (NotFoundException $e) {
            return false;
        }
    }    

    public function usernameExist ($username)
    {
        try {
            $model = $this->getWhereOne('username', $username);
            return $model['id'];
        } catch (NotFoundException $e) {
            return false;
        }
    }

    public function userExist ($id)
    {
        try {
            $model = $this->get($id);
            return $model['id'];
        } catch (NotFoundException $e) {
            return false;
        }
    }    

    public function customInsert ($data)
    {
        //create user
        $data['password'] = PasswordGenerator::make($data['password']);
        //return user
        return  parent::insert($data);
    }
    
    public function customUpdate ($id, $data)
    {
        //new password
        if ($data['password'] && strlen($data['password']) > 1) {
            $data['password'] = PasswordGenerator::make($data['password']);
        }
        
        return parent::update($id, $data);
    }
    
    public function transform ($data)
    {
        unset($data['password']);
        unset($data['token']);
        unset($data['role_id']);
        return $data;
    }
}