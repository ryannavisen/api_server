<?php namespace App\Repository;

use App\Adapter\MySqlAdapter;

class Base {
    protected $_table = '';
    protected $_adapter;

    public function __construct ()
    {
        $this->_adapter = new MySqlAdapter($this->_table);
    }

    public function get ($id)
    {
        return $this->_adapter->get($id);
    } 

    public function getAll ()
    {
        return $this->_adapter->getAll();
    }

    public function paginate ($page = 1, $limit = 10, $order = 'ASC')
    {
        return $this->_adapter->paginate($page, $limit, $order);
    }

    public function update ($id, $fields = [])
    {
        return $this->_adapter->update($id, $fields);
    }

    public function insert ($fields = [])
    {
        return $this->_adapter->insert($fields);
    }

    public function delete ($id)
    {
        return $this->_adapter->delete($id);
    }             

    public function getWhere ($field, $value)
    {
        return $this->_adapter->getWhere($field, $value);
    }

    public function getWhereOne ($field, $value)
    {
        return $this->_adapter->getWhereOne($field, $value);
    }

    public function transform ($data)
    {
        return $data;
    }

    public function query ($sql)
    {
        return $this->_adapter->query($sql);
    }
    
    public function getColumns ()
    {
        return $this->_adapter->getColumns();
    }

    public static function getAllTables ()
    {
        return $this->_adapter->getAllTables();
    }

    public function transaction ($transactionFunction)
    {
        return $this->_adapter->transaction($transactionFunction);
    }

    public function duplicate ($model)
    {
        return $this->_adapter->duplicate($model);
    }
}