<?php
namespace App\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

interface MiddlewareInterface {
    public static function next (Request $request, Application $app); 
}