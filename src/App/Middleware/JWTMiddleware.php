<?php
namespace App\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use App\Exceptions\InvalidTokenAccessException;
use App\Repository\Token;
use App\Repository\User;
use App\Repository\UserProfile;

class JWTMiddleware implements MiddlewareInterface {

    public static function next (Request $request, Application $app) 
    {
        $token = $request->headers->get('Authorization', '');
        // error_log($token);
        if(!$token) {
            $token = $request->query->get('Authorization', '');
            if (!$token) {
                throw new InvalidTokenAccessException('Invalid Access', null, []);
            }
            
        }
        // error_log('payload ' . $token);
        //steps check its valid token
        $payload = $app['jwt_service']->verifyToken($token);
        $data = $payload['data'];

        //get token, user_id
        $userId = (!empty($data->user_id)) ? $data->user_id : false;
        $organizationId = 1;
        $roleId = (!empty($data->role)) ? $data->role : false;

        if (!($userId && $organizationId && $roleId)) {
            throw new InvalidTokenAccessException('Invalid Token Payload', null, []);
        }

        //check token exist
        $tokenRepo = new Token($app);

        if (!$tokenRepo->tokenExist($token)) {
            throw new InvalidTokenAccessException('Blacklisted Token', null, []);
        }
        //check user exist
        $userRepo = new User();

        if (!$userRepo->userExist($userId)) {
            throw new InvalidTokenAccessException('Invalid User', null, []);
        }

        //save user to $app
        $app['user_id'] = $userId;
        $app['role_id'] = $roleId;
        return;
    }
}