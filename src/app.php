<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));
$app['translator.domains'] = include_once (__DIR__ . '/App/Locale/translation.php');
// $app['translator']->trans($message, array('%name%' => $name))
$app['base_url'] = 'http://localhost:8887';
$app['server_name'] = 'navisen_platform';

return $app;
