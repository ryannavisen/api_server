<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Silex\Application;
use App\Exceptions\ValidationException;
use App\Exceptions\InvalidTokenAccessException;
use App\Exceptions\TokenException;
use App\Service\JWTService;
use App\Middleware\JWTMiddleware;
use App\Lambda\Login;
use App\Lambda\AdminQuery;
use App\Lambda\MemberQuery;
use App\Lambda\BuildPackage;
use App\Lambda\Version;

use RedBeanPHP\Facade as R;
//Request::setTrustedProxies(array('127.0.0.1'));

$app->register(new JDesrosiers\Silex\Provider\CorsServiceProvider(), [
    "cors.allowOrigin" => "*",
]);

header_remove("X-Powered-By");
header('X-Key: HRdO2AAVLGK7nWhopwIz4vyTfwZvwiAjepyrc1kj');

//accepting JSON
$app->before(function (Request $request, Application $app) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

R::setup(
    $app['db.connection.default.rsn'],
    $app['db.connection.default.user'],
    $app['db.connection.default.password']
);
R::freeze( TRUE );
// R::fancyDebug( TRUE );

// R::startLogging(); //start logging
// $logs = R::getLogs(); //obtain logs
// R::addDatabase( 'DB1', 'sqlite:/tmp/d1.db', 'usr', 'pss', $frozen );
// R::selectDatabase( 'DB1' );

$app['jwt_service'] = function () use ($app) {
    return new JWTService($app);
};

$jwtAuthenticationMiddleware = function (Request $request, Application $app) 
{
    return JWTMiddleware::next($request, $app);
};

$app->get('/', function () use ($app) {
    return include_once(__DIR__ . '/../frontend/build/index.html');
});

$app->post('/api/login', 'App\Lambda\Login::index');
// $app->get('/api/affiliate', 'App\Lambda\Affiliate::index');
$app->get('/api/admin/method', 'App\Lambda\AdminQuery::methods')->before($jwtAuthenticationMiddleware);
// $app->post('/api/query', 'App\Lambda\AdminQuery::index')->before($jwtAuthenticationMiddleware);
// $app->get('/api/query', 'App\Lambda\AdminQuery::index')->before($jwtAuthenticationMiddleware);
$app->post('/api/query', 'App\Lambda\MemberQuery::index')->before($jwtAuthenticationMiddleware);
// $app->get('/api/query', 'App\Lambda\MemberQuery::index')->before($jwtAuthenticationMiddleware);
$app->get('/api/build/{id}', 'App\Lambda\BuildPackage::index')->before($jwtAuthenticationMiddleware);
// $app->get('/api/doc', 'App\Lambda\Doc::index');
$app->get('/api/version', 'App\Lambda\Version::index');

$app["cors-enabled"]($app);

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }
    error_log($e);
    $message = $e->getMessage();
    
    if ($e instanceof App\Exceptions\ValidationException) {
        $code = 403;
    }

    if ($e instanceof App\Exceptions\InvalidTokenAccessException) {
        $code = 401;
    }
    if ($e instanceof App\Exceptions\TokenException) {
        $code = 401;
    }
    
    // return $app -> json(['error' => $e -> getMessage(), 'type' => get_class($e)], $code);
    return $app->json(['error' => true, 'message' => $message], $code);
});