<?php

use Phinx\Migration\AbstractMigration;

class Token extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $tokens = $this->table('token');
        $tokens->addColumn('user_id', 'integer')
              ->addColumn('token_hash', 'string', array('limit' => 100))
              ->addColumn('token', 'string', array('limit' => 512))
              ->addColumn('action', 'integer')
              ->addColumn('data', 'text')
              ->addColumn('expire_at', 'integer')
              ->addColumn('created', 'datetime')
              ->addColumn('status', 'integer')
              ->addIndex(array('token_hash'), array('unique' => true))
              ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('token');
    }        
}
