<?php

use Phinx\Migration\AbstractMigration;

class AffiliateCodes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     public function change()
     {
         $relationships = $this->table('affiliate');
         $relationships->addColumn('user_id', 'integer')
                ->addColumn('percentage', 'float')
                ->addColumn('amount', 'float')
                ->addColumn('status', 'integer')
               ->addColumn('code', 'text')
               ->addColumn('created_at', 'datetime')
               ->create();
     }
 
     /**
      * Migrate Down.
      */
      public function down()
      {
          $this->dropTable('affiliate');
      }  
}
