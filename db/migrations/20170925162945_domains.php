<?php

use Phinx\Migration\AbstractMigration;

class Domains extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $domains = $this->table('domains');
        $domains->addColumn('user_id', 'integer')
              ->addColumn('domain_hash', 'string', array('limit' => 512))
              ->addColumn('domain', 'string', array('limit' => 512))
              ->addColumn('prebuild', 'text')
              ->addColumn('afterbuild', 'text')
              ->addColumn('version', 'string', array('limit' => 10))
              ->addColumn('status', 'integer')
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime')
              ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('domains');
    }
}
