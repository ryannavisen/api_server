<?php

use Phinx\Migration\AbstractMigration;

class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $users = $this->table('user');
        $users->addColumn('username', 'string', array('limit' => 100))
              ->addColumn('password', 'string', array('limit' => 255))
              ->addColumn('profile_id', 'integer')
              ->addColumn('email', 'string', array('limit' => 100))
              ->addColumn('profile_image', 'string', array('limit' => 255))
              ->addColumn('first_name', 'string', array('limit' => 30))
              ->addColumn('last_name', 'string', array('limit' => 30))
              ->addColumn('created', 'datetime')
              ->addColumn('updated', 'datetime', array('null' => true))
              ->addColumn('role_id', 'integer')
              ->addColumn('status', 'integer')
              ->addColumn('token', 'text')
              ->addIndex(array('username', 'email'), array('unique' => true))
              ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('user');
    }    
}
