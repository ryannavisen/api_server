<?php

use Phinx\Migration\AbstractMigration;

class UserProfile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $user_profiles = $this->table('userprofile');
        $user_profiles->addColumn('address', 'string', array('limit' => 255))
              ->addColumn('city', 'string')
              ->addColumn('state', 'string')
              ->addColumn('country', 'string')
              ->addColumn('postal_code', 'string', array('limit' => 10))
              ->addColumn('phone', 'integer')
              ->addColumn('fax', 'integer')
              ->addColumn('gender', 'integer')
              ->addColumn('age', 'integer')
              ->addColumn('birth_date', 'date')
              ->addColumn('last_login', 'datetime')
              ->addColumn('last_ip', 'string', array('limit' => 40))
              ->addColumn('terms_and_condition', 'boolean')
              ->addColumn('is_approved', 'boolean')
              ->addColumn('affilate_id', 'integer')
              ->addColumn('subscription_id', 'integer')
              ->addColumn('plan_id', 'integer')
              ->addColumn('user_id', 'integer')
              ->create();
    }
    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('userprofile');
    }            
}
