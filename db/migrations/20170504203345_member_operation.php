<?php

use Phinx\Migration\AbstractMigration;

class MemberOperation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $memberOperations = $this->table('memberoperation');
        $memberOperations->addColumn('action_date', 'datetime')
              ->addColumn('user_id', 'integer')
              ->addColumn('action', 'string', array('limit' => 40))
              ->addColumn('detail', 'string', array('limit' => 255))
              ->addColumn('last_ip', 'string', array('limit' => 40))
              ->addColumn('user_agent', 'string', array('limit' => 100))
              ->create();
    }


    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('memberoperation');
    }     
}
