<?php

use Phinx\Seed\AbstractSeed;
// use App\Utility\PasswordGenerator;
use App\Repository\UserProfileRepository;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
            'email' => 'ryan.wong@navisen.com',
            'username' => 'ryan.wong@navisen.com',
            'password' =>  password_hash('~' . 'test123A2W!' . '!', PASSWORD_BCRYPT, array('cost' => 11)),
            'profile_id' => 1,
            'profile_image' => 'profile.png',
            'first_name' => 'Ryan',
            'last_name' => 'Wong',
            'created' => date('Y-m-d h:m:s'),
            'updated' => date('Y-m-d h:m:s'),
            'role_id' => 1,
            'status' => 1,
            'token' => ''
            ],
            [
                'email' => 'demo@navisen.com',
                'username' => 'demo@navisen.com',
                'password' => password_hash('~' . 'test123A2W!' . '!', PASSWORD_BCRYPT, array('cost' => 11)),
                'profile_id' => 2,
                'profile_image' => 'profile.png',
                'first_name' => 'Demo',
                'last_name' => 'Demo',
                'created' => date('Y-m-d h:m:s'),
                'updated' => date('Y-m-d h:m:s'),
                'role_id' => 0,
                'status' => 1,
                'token' => ''
                ]
        ];
        $profile = [
            [
            'address' => '',
            'city' => '',
            'state' => '',
            'country' => '',
            'postal_code' => '',
            'phone' => 0,
            'fax' => 0,
            'gender' => 'N',
            'age' => 0,
            'birth_date' => date('Y-m-d'),
            'last_login' => date('Y-m-d h:m:s'),
            'last_ip' => '',
            'terms_and_condition' => 0,
            'is_approved' => 0,
            'affilate_id' => 0,
            'subscription_id' => 0,
            'plan_id' => 3,
            'user_id' => 1
            ],
            [
                'address' => '',
                'city' => '',
                'state' => '',
                'country' => '',
                'postal_code' => '',
                'phone' => 0,
                'fax' => 0,
                'gender' => 'N',
                'age' => 0,
                'birth_date' => date('Y-m-d'),
                'last_login' => date('Y-m-d h:m:s'),
                'last_ip' => '',
                'terms_and_condition' => 0,
                'is_approved' => 0,
                'affilate_id' => 0,
                'subscription_id' => 0,
                'plan_id' => 1,
                'user_id' => 2
                ]
        ];
        $posts = $this->table('user');
        $posts->insert($data)->save();

        $profiles = $this->table('userprofile');
        $profiles->insert($profile)->save();
    }
}
