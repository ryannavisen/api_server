<?php

use Phinx\Seed\AbstractSeed;

class EmailTemplateSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'slug'    => 'register',
                'from'    => 'admin@navisen.com',
                'subject'    => 'Thank you for ordering',
                'html' => ''
            ),
            array(
                'slug'    => 'reset-password',
                'from'    => 'admin@navisen.com',
                'subject'    => 'Reset Password',
                'html' => ''
            )
        );
        $posts = $this->table('emailtemplate');
        $posts->insert($data)->save();
    }
}
