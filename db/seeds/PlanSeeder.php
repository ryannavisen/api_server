<?php

use Phinx\Seed\AbstractSeed;

class PlanSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'name'    => 'basic',
                'ref_id'  => 1,
                'min' => 1,
                'max' => 2
            ),
            array(
                'name'    => 'multi license',
                'ref_id'  => 2,
                'min' => 2,
                'max' => 11
            ),
            array(
                'name'    => 'unlimited',
                'ref_id'  => 3,
                'min' => 0,
                'max' => 10000000
            ),
            array(
                'name'    => 'enterprise',
                'ref_id'  => 4,
                'min' => 0,
                'max' => 10000000
            ),            
        );
        $settings = $this->table('plan');
        $settings->insert($data)->save();
    }
}
