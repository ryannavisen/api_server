<?php

use Phinx\Seed\AbstractSeed;

class SettingSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'name'    => 'maintenance',
                'value'  => '0'
            ),
            array(
                'name'    => 'version',
                'value'  => '1.0.0'
            ),
            array(
                'name'    => 'env',
                'value'  => 'dev'
            ),
            array(
                'name'    => 'lock',
                'value'  => '0'
            ),            
        );
        $settings = $this->table('settings');
        $settings->insert($data)->save();
    }
}
