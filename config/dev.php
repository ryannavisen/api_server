<?php

use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;

// include the prod configuration
require __DIR__.'/prod.php';

// enable the debug mode
$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/silex_dev.log',
));

$app->register(new WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
));

$app['jwt.options'] = [
    'expireAt' => 216000,
    'alg' => 'HS256',
    'key' => 'TLU65GIy5sWOlDT1XmqN0zgI'
];

$app['db.connection.default.driver'] = "mysql";
$app['db.connection.default.host'] = "localhost";
$app['db.connection.default.name'] = "navisen";
$app['db.connection.default.user'] = "root";
$app['db.connection.default.password'] = "";